const { sumtwo, longTask } = require("./test.js")
const axios = require("axios")

describe("test file tests", () => {
    it("should sum two numbers", () => {
        expect(sumtwo(1,2)).toBe(3)
    })
    it("should do a long task", async () => {
        expect(await longTask()).toBe(1)
    })
    it("should do a long task", async () => {
        expect(await longTask()).toBe(1)
    })
    it("should do a long task", async () => {
        expect(await longTask()).toBe(1)
    })
    it("should do a long task", async () => {
        expect(await longTask()).toBe(1)
    })
    it("should do a long task", async () => {
        expect(await longTask()).toBe(1)
    })
})