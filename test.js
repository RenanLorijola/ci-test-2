const sumtwo = (a, b) => a+b

const longTask = async () => {
    await new Promise(resolve => setTimeout(resolve, 4000));
    return 1; 
}

module.exports = { sumtwo, longTask }