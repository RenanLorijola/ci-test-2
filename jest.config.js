const config = {
    verbose: true,
    testRegex: ".*\.spec\.js"
};

module.exports = config;